namespace OnlineShop.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class changeTableOrder : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Orders", "Note", c => c.String(maxLength: 500));
            AddColumn("dbo.Orders", "PaymentMethod", c => c.String(maxLength: 256));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Orders", "PaymentMethod");
            DropColumn("dbo.Orders", "Note");
        }
    }
}
