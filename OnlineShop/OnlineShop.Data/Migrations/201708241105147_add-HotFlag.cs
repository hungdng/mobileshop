namespace OnlineShop.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addHotFlag : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Products", "HotFlag", c => c.Boolean());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Products", "HotFlag");
        }
    }
}
