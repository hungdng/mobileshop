﻿namespace OnlineShop.Data.Migrations
{
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using OnlineShop.Data.Models;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<OnlineShopDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(OnlineShopDbContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //

            //CreateUserType(context);

            CreateUser(context);
        }

        //public void CreateUserType(OnlineShopDbContext context)
        //{
        //    if (context.UserTypes.Count() == 0)
        //    {
        //        context.UserTypes.AddRange(new List<UserType>()
        //        {
        //            new UserType() { UserTypeID = "EMPLOYEE", UserTypeName = "Nhân Viên", Description = "Là loại nhân viên", CreatedDate = DateTime.Now, UpdatedDate = DateTime.Now},
        //            new UserType() { UserTypeID = "CUSTOMER", UserTypeName = "Khách Hàng", Description = "Là loại Khách hàng", CreatedDate = DateTime.Now, UpdatedDate = DateTime.Now}
        //        });
        //    }
        //    context.SaveChanges();
        //}

        private void CreateUser(OnlineShopDbContext context)
        {
            var manager = new UserManager<AppUser>(new UserStore<AppUser>(new OnlineShopDbContext()));
            if (manager.Users.Count() == 0)
            {
                var roleManager = new RoleManager<AppRole>(new RoleStore<AppRole>(new OnlineShopDbContext()));

                var user = new AppUser()
                {
                    UserName = "hungdng.92@gmail.com",
                    Email = "hungdng.92@gmail.com",
                    EmailConfirmed = true,
                    BirthDay = DateTime.Now,
                    FullName = "Tran Anh Hung",
                    Gender = "Nam"
                };
                if (manager.Users.Count(x => x.UserName == "admin") == 0)
                {
                    manager.Create(user, "123456");

                    if (!roleManager.Roles.Any())
                    {
                        roleManager.Create(new AppRole { Name = "Admin", Description = "Quản trị viên" });
                        roleManager.Create(new AppRole { Name = "Member", Description = "Người dùng" });
                    }

                    var adminUser = manager.FindByName("admin");

                    manager.AddToRoles(adminUser.Id, new string[] { "Admin", "Member" });
                }
            }
        }
    }
}
