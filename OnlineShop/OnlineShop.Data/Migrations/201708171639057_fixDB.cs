namespace OnlineShop.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class fixDB : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.AppUsers", "UserTypeID", "dbo.UserTypes");
            DropIndex("dbo.AppUsers", new[] { "UserTypeID" });
            AddColumn("dbo.AppRoles", "IsDeleted", c => c.Boolean());
            AddColumn("dbo.ProductCategories", "DisplayOrder", c => c.Int());
            AddColumn("dbo.ProductCategories", "Status", c => c.Boolean(nullable: false));
            AddColumn("dbo.AppUsers", "IsDeleted", c => c.Boolean(nullable: false));
            AlterColumn("dbo.AppUsers", "Gender", c => c.String());
            DropColumn("dbo.AppUsers", "UserTypeID");
            DropTable("dbo.UserTypes");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.UserTypes",
                c => new
                    {
                        UserTypeID = c.String(nullable: false, maxLength: 50),
                        UserTypeName = c.String(nullable: false, maxLength: 256),
                        Description = c.String(maxLength: 500),
                        CreatedDate = c.DateTime(),
                        UpdatedDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.UserTypeID);
            
            AddColumn("dbo.AppUsers", "UserTypeID", c => c.String(maxLength: 50));
            AlterColumn("dbo.AppUsers", "Gender", c => c.Boolean());
            DropColumn("dbo.AppUsers", "IsDeleted");
            DropColumn("dbo.ProductCategories", "Status");
            DropColumn("dbo.ProductCategories", "DisplayOrder");
            DropColumn("dbo.AppRoles", "IsDeleted");
            CreateIndex("dbo.AppUsers", "UserTypeID");
            AddForeignKey("dbo.AppUsers", "UserTypeID", "dbo.UserTypes", "UserTypeID");
        }
    }
}
