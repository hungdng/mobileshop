namespace OnlineShop.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addParentID : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ProductCategories", "ParentID", c => c.Guid());
        }
        
        public override void Down()
        {
            DropColumn("dbo.ProductCategories", "ParentID");
        }
    }
}
