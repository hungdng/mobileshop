namespace OnlineShop.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class change_Nullable_HotFlag_Product : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Products", "HotFlag", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Products", "HotFlag", c => c.Boolean());
        }
    }
}
