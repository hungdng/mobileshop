namespace OnlineShop.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addTableManufacture : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Manufactures",
                c => new
                    {
                        ManufactureID = c.Guid(nullable: false),
                        ManufactureName = c.String(),
                        Logo = c.String(),
                        Description = c.String(),
                        Url = c.String(),
                    })
                .PrimaryKey(t => t.ManufactureID);
            
            AddColumn("dbo.Products", "ManufactureID", c => c.Guid(nullable: false));
            CreateIndex("dbo.Products", "ManufactureID");
            AddForeignKey("dbo.Products", "ManufactureID", "dbo.Manufactures", "ManufactureID", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Products", "ManufactureID", "dbo.Manufactures");
            DropIndex("dbo.Products", new[] { "ManufactureID" });
            DropColumn("dbo.Products", "ManufactureID");
            DropTable("dbo.Manufactures");
        }
    }
}
